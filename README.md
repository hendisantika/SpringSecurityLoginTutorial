# SpringSecurityLoginTutorial

1. mvn clean
2. mvn clean install
3. Go to the target folder
4. java -jar login-demo-0.0.1-SNAPSHOT.jar

- http://localhost:8080/registration
- http://localhost:8080/login

#### Screenshot

Login Page

![Login Page](img/login.png "Login Page")

Registration Page

![Registration Page](img/registration.png "Registration Page")

Admin Page

![Admin Page](img/admin.png "Admin Page")

